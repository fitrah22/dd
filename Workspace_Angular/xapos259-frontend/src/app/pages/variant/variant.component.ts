import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Category } from 'src/app/model/category';
import { Variant } from 'src/app/model/variant';
import { CategoryService } from 'src/app/service/category.service';
import { VariantService } from 'src/app/service/variant.service';

@Component({
  selector: 'app-variant',
  templateUrl: './variant.component.html',
  styleUrls: ['./variant.component.css']
})
export class VariantComponent implements OnInit {
  public variant: Variant[] = [];
  public category: Category[] = [];
  public editVariant: Variant;
  public deleteVariant: Variant;
  public selectedCategory = 0;

  constructor(private variantService: VariantService, private categoryService: CategoryService) {
    this.editVariant = {} as Variant;
    this.deleteVariant = {} as Variant;
   }

  ngOnInit(): void {
    this.getVariant();
  }

  public getCategory(): void {
    this.categoryService.getCategory().subscribe(
      (response: Category[]) => {
        this.category = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
      )
  }

  public getVariant(): void {
    this.variantService.getVariant().subscribe(
      (response: Variant[]) => {
        this.variant = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }

  public onAddVariant(addForm: NgForm): void {
    document.getElementById('add-variant-form');
    this.variantService.addVariant(addForm.value).subscribe(
      (response: Variant) => {
        console.log(response);
        this.getVariant();
        addForm.reset();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
        addForm.reset();
      }
    )
  }

  public onEditVariant(variant: Variant): void {
    this.variantService.editVariant(variant).subscribe(
      (response: Variant) => {
        console.log(response);
        this.getVariant();
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
        alert(error.message);
      }
    )
  }

  public onDeleteVariant(id: number): void {
    this.variantService.deleteVariant(id).subscribe(
      (response: void) => {
        console.log(response);
        this.getVariant();
      },
      (error: HttpErrorResponse) => {
        console.log(error.message);
        alert(error.message);
      }
    )
  }

  public onOpenModal(variant: Variant, mode: string): void {
    const container = document.getElementById('main-container');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle', 'modal');

    if(mode === 'add') {
      this.getCategory();
      button.setAttribute('data-target', '#addVariantModal');
    }

    if(mode === 'edit') {
      console.log('edit')
      this.getCategory();
      this.editVariant = variant;
      this.selectedCategory = this.editVariant.categoryId;
      button.setAttribute('data-target', '#editVariantModal');
    }

    if(mode === 'delete') {
      console.log('delete')
      this.deleteVariant = variant;
      button.setAttribute('data-target', '#deleteVariantModal');
    }

    container!.appendChild(button);
    button.click();
  }

}
