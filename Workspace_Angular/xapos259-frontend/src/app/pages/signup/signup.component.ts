import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Regis } from 'src/app/model/regis';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  public regis: Regis = new Regis();

  constructor(private authService: AuthService, private router: Router) { }

  moveToLogin() {
    this.router.navigate(['login']);
  }
  ngOnInit(): void {
 }
  register () {
    this.authService.register(this.regis).subscribe(
      (data: any) => {
        this.router.navigate(['login']);
      },
      (err) => {
        console.log(err.error);
      },
      () => {
      console.log('Register Successfully');
      alert('Register Successfully');
    }
    )
  }
}
