import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Regis } from 'src/app/model/regis';
import { User } from 'src/app/model/user';
import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public user: User = new User();
  public regis: Regis = new Regis();

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

   
  moveToRegister() {
    this.router.navigate(['signup']);
  }

  login() {
    this.authService.login(this.user).subscribe(
      (data: any) => {
        console.log(data);
        localStorage.setItem('token', data.token);
        this.router.navigate(['home']);
      },
      (err) => {
        console.log(err.error);
      },
        () => {
          console.log('Login Successfully');
          alert('Register Successfully');
        }
    )
  }
}
