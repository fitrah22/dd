import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "../guards/auth.guard";
import { CategoryComponent } from "../pages/category/category.component";
import { HomeComponent } from "../pages/home/home.component";
import { LoginComponent } from "../pages/login/login.component";
import { ProductComponent } from "../pages/product/product.component";
import { SignupComponent } from "../pages/signup/signup.component";
import { VariantComponent } from "../pages/variant/variant.component";

const route: Routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    { path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
    { path: 'signup', component: SignupComponent}, 
    { path: 'category', component: CategoryComponent, canActivate: [AuthGuard]},
    { path: 'variant', component: VariantComponent, canActivate: [AuthGuard]},
    { path: 'product', component: ProductComponent, canActivate: [AuthGuard]}
]

@NgModule({
  imports: [
      RouterModule.forRoot(route, {
          initialNavigation: 'enabled',
      }),
  ],
  exports: [RouterModule], 
})
export class AppRoutingModule {}