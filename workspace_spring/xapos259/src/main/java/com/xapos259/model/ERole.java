package com.xapos259.model;

//enum untuk menyimpan data statik
public enum ERole {
	ROLE_USER,
	ROLE_MODERATOR,
	ROLE_ADMIN
}
